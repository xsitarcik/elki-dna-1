package de.lmu.ifi.dbs.elki.datasource.filter.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * 
 * @author Tomas Farkas
 *
 */
public class DNA_FHT_Filter_Test {

 /* @Test
  public void testSpectrumMaker(){
    List<Double> resAc = new ArrayList<>();
    List<Double> resEx = new ArrayList<>();
    resEx.add(36.);
    resEx.add(42.);
    resEx.add(27.);
    resEx.add(15.);
    resEx.add(16.);

    float[] data = {16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1};
    
    new DNA_FHT_Filter(null, 0).addPowerSpectrum(data, resAc);
    
    assertEquals(resEx, resAc);
  }*/
}
