package de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider;

import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;

public class BSI_DNA_Numerifier implements DNA_Numerifier {

  int out_cardinality;
  
  protected BSI_DNA_Numerifier(int card) {
    super();
    this.out_cardinality = card;
  }

  @Override
  public float[][] numerify(char[] inSeq, boolean dense) {

    int length = inSeq.length;
    if(dense) {
      length *= 4;
    }
    float[][] outVecs = new float[out_cardinality][];
    
    if(!dense) {

      for(int b = 0; b < out_cardinality; b++) {

        float[] bsi = new float[length];
        outVecs[b] = bsi;
        
        for(int i = 0; i < inSeq.length; i++) {
          char curB = inSeq[i];
          if(curB == b) {
            bsi[i] = 1;
          }
        }
      }

    }
    else {

      for(int b = 0; b < out_cardinality; b++) {

        float[] bsi = new float[length];
        outVecs[b] = bsi;

        for(int i = 0; i < inSeq.length; i++) {
          char curB = inSeq[i];
          for(int j = 0; j < 4; j++) {
            char curb = (char) (curB & 4);
            curB = (char) (curB >> 2);

            if(curb == b) {
              bsi[i * 4 + j] = 1;
            }
          }
        }
      }

    }
    
    return outVecs;
  }
  
  public static class Parameterizer extends AbstractParameterizer {

    protected static final OptionID OUT_CARD = new OptionID("numerifier.cardinality", "Number of BSIs to create");
    int card;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      final IntParameter param = new IntParameter(OUT_CARD);
      if(config.grab(param)) {
        card = param.getValue();
      }      
    }  
    
    @Override
    protected Object makeInstance() {
      return new BSI_DNA_Numerifier(card);
    }
  }

}
