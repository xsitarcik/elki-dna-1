package de.lmu.ifi.dbs.elki.result.stuba;

import de.lmu.ifi.dbs.elki.result.Result;

/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 * @author Tomas Farkas
 *
 */
public class StringResult implements Result{

  String data;
  String name = "StringResult";
  
  public StringResult(String data){
    super();
    this.data = data;
  }
    
  public StringResult(String data, String name){
    super();
    this.data = data;
    this.name = name;
  }
  
  @Override
  public String getLongName() {
    return name;
  }

  @Override
  public String getShortName() {
    return name;
  }

  public String getContent(){
    return data;
  }
  
}
