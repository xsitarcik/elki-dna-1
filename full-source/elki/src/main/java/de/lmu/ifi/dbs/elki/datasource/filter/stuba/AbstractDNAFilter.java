package de.lmu.ifi.dbs.elki.datasource.filter.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.List;

import de.lmu.ifi.dbs.elki.data.FeatureVector;
import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.stuba.MultipleTypeVectorList;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.type.SimpleTypeInformation;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.filter.ObjectFilter;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.RandomDNADataProvider;
import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.parallel.stuba.ParallelFilter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;

/**
 * Abstract data filter that handles input of DNA data
 * 
 * @author Tomas Farkas
 */
public abstract class AbstractDNAFilter implements ObjectFilter, ParallelFilter {

  protected final DNA_DataProvider provider;

  protected static final Logging LOG = Logging.getLogger(AbstractDNAFilter.class);

  protected MultipleObjectsBundle inputBundle;

  protected MultipleObjectsBundle res;

  protected List<? extends StringVector_externalID> inputVectors = null;

  protected LabelList[] labels;

  @SuppressWarnings("rawtypes")
  protected FeatureVector[] data;

  protected AbstractDNAFilter(DNA_DataProvider provider) {
    this.provider = provider;
  }

  @Override
  public MultipleObjectsBundle filter(MultipleObjectsBundle objects) {
    init(objects);
    for(int i = 0; i < getNrOfTasks(); i++) {
      processTask(i);
    }
    return cleanUpAndReturn();
  }

  @SuppressWarnings("unchecked")
  @Override
  public void init(MultipleObjectsBundle objects) {
    inputBundle = objects;
    res = new MultipleObjectsBundle();
    int validEntryColumnCount = 0;
   
    for(int r = 0; r < objects.metaLength(); r++) {
      final SimpleTypeInformation<?> type = objects.meta(r);
      final List<?> column = objects.getColumn(r);      

     if(TypeUtil.STRING_VECTOR_EXTERNAL_MULTIPLE.isAssignableFromType(type)){
        validEntryColumnCount++;
        inputVectors = ((List<? extends StringVector_externalID>) column);
        data = new MultipleTypeVectorList[getNrOfTasks()];
      }else if(TypeUtil.STRING_VECTOR_EXTERNAL.isAssignableFromType(type)){
        validEntryColumnCount++;
        inputVectors = ((List<? extends StringVector_externalID>) column);
        //data = new T[getNrOfTasks()];    
        createArray_hook(); 
      }else{
        res.appendColumn(type, column);
        
      }     
      
    }
    if(validEntryColumnCount != 1) {
      LOG.warning("Invalid input parameter count for DNA filter");
    }
    
    labels = new LabelList[getNrOfTasks()];         
  }

  protected abstract void createArray_hook();

  @Override
  public int getNrOfTasks() {
    return inputBundle.dataLength();
  }

  public abstract static class Parameterizer extends AbstractParameterizer {

    protected static final OptionID DATA_PROVIDER_ID = new OptionID("filter.dataProvider", "Data provider providing DNA 2-bit encoded string array");

    protected DNA_DataProvider provider = null;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      final ObjectParameter<DNA_DataProvider> param = new ObjectParameter<>(DATA_PROVIDER_ID, DNA_DataProvider.class, RandomDNADataProvider.class);
      if(config.grab(param)) {
        provider = (DNA_DataProvider) param.instantiateClass(config);
      }
    }
  }
}
