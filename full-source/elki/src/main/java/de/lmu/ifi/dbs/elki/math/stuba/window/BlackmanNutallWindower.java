package de.lmu.ifi.dbs.elki.math.stuba.window;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * @author Tomas Farkas
 *
 */
public class BlackmanNutallWindower implements TransformVectorWindower {

  private static final float a0 = 0.3635819f;
  private static final float a1 = 0.4891775f;
  private static final float a2 = 0.1365995f;
  private static final float a3 = 0.0106411f;
  
  @Override
  public float[] applyWindow(float[] data, int length) {
    
    if(data.length<length){
      throw new IllegalArgumentException("Requested window size is gerater than the data length");
    }    
    for(int i=0;i<length;i++){
      float coef = (float) (a0-a1*Math.cos((2*Math.PI*(i))/(length-1)) + a2*Math.cos((4*Math.PI*(i))/(length-1))- a3 *Math.cos((6*Math.PI*(i))/(length-1)));
      data[i] = data[i] * coef;
    }
    return data;
  }

}
