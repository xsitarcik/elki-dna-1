package de.lmu.ifi.dbs.elki.distance.distancefunction.stuba;

import java.util.ArrayList;
import java.util.List;

import de.lmu.ifi.dbs.elki.data.SparseNumberVector;
import de.lmu.ifi.dbs.elki.data.stuba.MultipleTypeVectorList;
import de.lmu.ifi.dbs.elki.data.type.SimpleTypeInformation;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.parser.ArffParser;
import de.lmu.ifi.dbs.elki.distance.distancefunction.AbstractPrimitiveDistanceFunction;
import de.lmu.ifi.dbs.elki.distance.distancefunction.PrimitiveDistanceFunction;
import de.lmu.ifi.dbs.elki.distance.distancefunction.stuba.SparseVectorComparator.SparseCompareType;
import de.lmu.ifi.dbs.elki.logging.Logging;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.DoubleListParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.EnumParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectListParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;

/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 * @author Tomas
 * @param <PrimitiveDistanceFunction>
 *
 */
@SuppressWarnings("rawtypes")
public class MultipleTypeVectorComparator extends AbstractPrimitiveDistanceFunction<MultipleTypeVectorList> {

  private static Logging LOG = Logging.getLogger(MultipleTypeVectorComparator.class);
  List<PrimitiveDistanceFunction> fList;
  List<Double> wList;


  public MultipleTypeVectorComparator(List<PrimitiveDistanceFunction> fList, List<Double> wList) {
    this.fList = fList;
    this.wList = wList;
    
    if(wList == null){
      this.wList = new ArrayList<Double>();
      for(int i=0;i<fList.size();i++){
        this.wList.add(1.0);
      }      
    }else if(wList.size()<fList.size()){
      for(int i=wList.size();i<fList.size();i++){
        this.wList.add(1.0);
      } 
    }
  }

  @Override
  public SimpleTypeInformation<? super MultipleTypeVectorList> getInputTypeRestriction() {
    return (SimpleTypeInformation<? super MultipleTypeVectorList>) TypeUtil.MULTIPLE_TYPE_VECTOR;
  }

  @Override
  public double distance(MultipleTypeVectorList o1, MultipleTypeVectorList o2) {
    if(o1.getDimensionality()!=o2.getDimensionality() || o1.getDimensionality()!=fList.size()){
      LOG.error("Wrong number of distance functions specified");
    }
    
    double distance = 0;
    
    for(int i=0;i<fList.size();i++){
      double pDist = wList.get(i) * fList.get(i).distance(o1.getValue(i), o2.getValue(i));
      distance += pDist;
      //System.out.println(pDist);
    }
    
    return distance;
    
  }

  public static class Parameterizer<O> extends AbstractParameterizer {

    protected static final OptionID DISTFs = new OptionID("dist.functions", "List of distance functions to use for vector fields");
    protected static final OptionID WEIGHTs = new OptionID("dist.weights", "List of weights of input vectors on result");

    List<PrimitiveDistanceFunction> fList;
    List<Double> wList;


    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      ObjectListParameter<PrimitiveDistanceFunction> fListP = new ObjectListParameter(DISTFs, PrimitiveDistanceFunction.class);
      if(config.grab(fListP)) {
        fList = fListP.instantiateClasses(config);
      }
      DoubleListParameter wListP = new DoubleListParameter(WEIGHTs, true);
      if(config.grab(wListP)) {
        wList = (List<Double>) wListP.getGivenValue();
      }
      
    }

    @Override
    protected Object makeInstance() {
      return new MultipleTypeVectorComparator(fList, wList);
    }

  }

}
