package de.lmu.ifi.dbs.elki.datasource.filter.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.lmu.ifi.dbs.elki.data.DoubleVector;
import de.lmu.ifi.dbs.elki.data.FeatureVector;
import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.stuba.MultipleTypeVectorList;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalMultipleID;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterEqualConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.LessEqualConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;

/**
 * Filter to create DNA word count histogram out of StringVector_externalID
 * description
 * 
 * @author Tomas Farkas
 * @tested 24/7/16 OK working with DNA data with 4 bases encoded in 1 byte
 */
public class DNA_WordFrequencyFilter extends AbstractDNAFilter {

  protected int wordLength;

  protected DNA_WordFrequencyFilter(DNA_DataProvider provider, int wordLength) {
    super(provider);
    this.wordLength = wordLength;
  }

  @Override
  protected void createArray_hook() {
    data = new DoubleVector[getNrOfTasks()];
  }

  @Override
  public void processTask(int inx) {
    StringVector_externalID currentItem = inputVectors.get(inx);
    labels[inx] = LabelList.make(Arrays.asList(currentItem.getLabel()));

    if(currentItem instanceof StringVector_externalMultipleID) {
      data[inx] = processTask((StringVector_externalMultipleID) currentItem);
    }
    else {
      data[inx] = processTask(currentItem);
    }
  }

  protected DoubleVector processTask(StringVector_externalID currentItem) {

    String fname = currentItem.getFileName();
    char[] dataString = provider.getData(fname, true, currentItem.getParams());
    if(dataString != null) {
      double[] frequencies = getFrequencies(dataString);
      return new DoubleVector(frequencies);
    }
    else {
      LOG.warning("could not find data for identifier: " + Arrays.toString(currentItem.getParams()));
      return new DoubleVector(new double[0]);
    }
  }

  @SuppressWarnings("rawtypes")
  protected MultipleTypeVectorList processTask(StringVector_externalMultipleID currentItem) {

    int fNum = currentItem.getNumberOfFiles();
    List<FeatureVector> result = new ArrayList<>();

    for(int f = 0; f < fNum; f++) {

      String fname = currentItem.getFileName(f);
      char[] dataString = provider.getData(fname, true, currentItem.getParams(f));
      if(dataString != null) {
        double[] frequencies = getFrequencies(dataString);
        result.add(new DoubleVector(frequencies));
      }
      else {
        LOG.warning("could not find data for identifier: " + currentItem.getFileName(f) + " " + Arrays.toString(currentItem.getParams(f)));
        result.add(new DoubleVector(new double[0]));
      }
    }

    return new MultipleTypeVectorList(result);
  }

  protected double[] getFrequencies(char[] dataString) {
    int vecLength = 4 << (wordLength - 1) * 2; // 4^wordLength
    // int lastBytes = Integer.MAX_VALUE >> (Integer.BYTES*8 - 2*(wordLength-1)
    // - 1); //last n-1 1s value
    int lastBytes = 0;
    for(int i = 0; i < wordLength; i++) {
      lastBytes = (lastBytes << 2) + 3;
    }

    int[] counts = new int[vecLength];
    double[] res = new double[vecLength];
    long totalCount = 0;

    int curWord = 0;
    // count stream
    for(int i = 0; i < dataString.length; i++) {
      char cur4B = dataString[i];
      for(int j = 0; j < 4; j++) {
        // System.out.println(curWord);
        counts[curWord]++;
        totalCount++;

        char curB = (char) ((cur4B & 192) >> 6); // current base = upper 2 bits
        cur4B = (char) (cur4B << 2);

        curWord = (curWord << 2) & (lastBytes);
        curWord += curB;
      }
    }
    // add last
    counts[curWord]++;
    totalCount++;
    // remove first incomplete words
    curWord = 0;
    int n = 0;
    for(int i = 0;; i++) {
      char cur4B = dataString[i];
      for(int j = 0; j < 4; j++) {
        counts[curWord]--;
        totalCount--;
        n++;
        if(n == wordLength) {
          break;
        }
        char curB = (char) ((cur4B & 192) >> 6); // current base = upper 2 bits
        cur4B = (char) (cur4B << 2);
        curWord = curWord << 2 & lastBytes;
        curWord += curB;
      }
      if(n == wordLength) {
        break;
      }
    }

    // normalize
    for(int i = 0; i < vecLength; i++) {
      double d = counts[i] * 1000.0 / totalCount;
      res[i] = d;
    }

    // return
    return res;
  }

  @Override
  public MultipleObjectsBundle cleanUpAndReturn() {
    res.appendColumn(TypeUtil.LABELLIST, Arrays.asList(labels));
    if(data.getClass().getComponentType().equals(DoubleVector.class)) {
      res.appendColumn(TypeUtil.DOUBLE_VECTOR_FIELD, Arrays.asList(data));
    }
    else {
      res.appendColumn(TypeUtil.MULTIPLE_TYPE_VECTOR, Arrays.asList(data));
    }
    return res;
  }

  public static class Parameterizer extends AbstractDNAFilter.Parameterizer {

    protected static final OptionID WORD_LENGTH = new OptionID("filter.wordLength", "Length of words considered");

    protected int wLen = 1;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      IntParameter wLenP = new IntParameter(WORD_LENGTH, 1);
      wLenP.addConstraint(new GreaterEqualConstraint(1));
      wLenP.addConstraint(new LessEqualConstraint(16));
      if(config.grab(wLenP)) {
        wLen = wLenP.getValue();
      }
    }

    @Override
    protected DNA_WordFrequencyFilter makeInstance() {
      return new DNA_WordFrequencyFilter(provider, wLen);
    }
  }

}
