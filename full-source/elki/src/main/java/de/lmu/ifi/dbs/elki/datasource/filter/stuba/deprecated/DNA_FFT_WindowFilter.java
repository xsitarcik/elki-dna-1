package de.lmu.ifi.dbs.elki.datasource.filter.stuba.deprecated;

import java.util.Arrays;

import de.lmu.ifi.dbs.elki.data.FloatVector;
import de.lmu.ifi.dbs.elki.data.LabelList;
import de.lmu.ifi.dbs.elki.data.stuba.StringVector_externalID;
import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.filter.stuba.AbstractDNAFilter;
import de.lmu.ifi.dbs.elki.datasource.filter.stuba.AbstractDNAFilter.Parameterizer;
import de.lmu.ifi.dbs.elki.datasource.stuba.DNAdataProvider.DNA_DataProvider;
import de.lmu.ifi.dbs.elki.math.stuba.impl.FFTbase;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;

/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-UniversitĆ¤t MĆ¼nchen
 Lehr- und Forschungseinheit fĆ¼r Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 * 
 * DEPRECATED - use ST_Filter instead
 * @author Tomas Farkas
 *
 */
public class DNA_FFT_WindowFilter extends AbstractDNAFilter {

  private int windowSize;

  private int windowShift;

  protected DNA_FFT_WindowFilter(DNA_DataProvider provider, int windowSize, int windowShift) {
    super(provider);
    this.windowSize = windowSize;
    this.windowShift = windowShift;
  }

  @Override
  public void processTask(int inx) {
    StringVector_externalID currentItem = inputVectors.get(inx);

    labels[inx] = LabelList.make(Arrays.asList(currentItem.getLabel()));
    String fname = currentItem.getFileName();
    char[] dataString = provider.getData(fname, false, currentItem.getParams());

    float[] result = new float[windowSize];
    // get FFT by windows
    for(int i = 0; i < dataString.length - windowSize; i += windowShift) {
      char[] actualIndicators = String.copyValueOf(dataString, i, windowSize).toCharArray();
      float[] actualRes = getSpectra(actualIndicators);
      for(int j = 0; j < windowSize; j++) {
        result[j] += actualRes[j];
      }
    }
    // normalize result
    for(int i = 0; i < result.length; i++) {
      result[i] = result[i] / dataString.length;
    }
    // return
    data[inx] = new FloatVector(result);
  }

  private float[] getSpectra(char[] data) {

    int length = data.length;

    float re[] = new float[length];
    float[] im = new float[length];
    for(int i = 0; i < length; i++) {
      char curB = data[i];
      if(curB == 0) {
        re[i] = 1;
      }
      else if(curB == 1) {
        re[i] = -1;
      }
      else if(curB == 2) {
        im[i] = 1;
      }
      else if(curB == 3) {
        im[i] = -1;
      }
    }
    FFTbase fft = new FFTbase(length, false);
    fft.fft(re, im);

    for(int i = 0; i < length; i++) {
      re[i] = re[i] * re[i] + im[i] * im[i];
    }

    return re;
  }

  @Override
  public MultipleObjectsBundle cleanUpAndReturn() {
    res.appendColumn(TypeUtil.LABELLIST, Arrays.asList(labels));
    res.appendColumn(TypeUtil.SPARSE_VECTOR_FIELD, Arrays.asList(data));
    return res;
  }

  @Override
  protected void createArray_hook() {
    data = new FloatVector[getNrOfTasks()];
  }

  public static class Parametrizer extends AbstractDNAFilter.Parameterizer {

    protected static final OptionID WINW_SIZE = new OptionID("filter.windowSize", "Size of sliding window, power of 2");

    protected static final OptionID WINW_SHIFT = new OptionID("filter.windowShift", "Window shift in 1 step");

    private int windowSize;

    private int windowShift;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      IntParameter winSP = new IntParameter(WINW_SIZE);
      winSP.addConstraint(new GreaterConstraint(0));

      IntParameter winSH = new IntParameter(WINW_SHIFT);
      winSH.addConstraint(new GreaterConstraint(0));

      if(config.grab(winSP)) {
        windowSize = winSP.getValue();
      }

      if(config.grab(winSH)) {
        windowShift = winSH.getValue();
      }
    }

    @Override
    protected Object makeInstance() {
      return new DNA_FFT_WindowFilter(provider, windowSize, windowShift);
    }

  }

}