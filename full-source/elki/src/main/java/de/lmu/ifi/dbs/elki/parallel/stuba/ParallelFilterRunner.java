package de.lmu.ifi.dbs.elki.parallel.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.filter.ObjectFilter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.constraints.GreaterEqualConstraint;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.IntParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.LongParameter;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;

/**
 * Universal reflective implementation of Processor interface
 * 
 * @author Tomas Farkas
 *
 */
public class ParallelFilterRunner implements ObjectFilter {

  int tpSize;
  long memSize;
  ParallelFilter filter;
  int step;

  public ParallelFilterRunner(int tpSize, long memSize, ParallelFilter filter, int step) {
    this.tpSize = tpSize;
    this.memSize = memSize;
    this.filter = filter;
    this.step = step;
  }

  private List<Runner> threads = Collections.synchronizedList(new ArrayList<Runner>());
  private int threadInx = 0;
  
  @Override
  public MultipleObjectsBundle filter(MultipleObjectsBundle objects) {
    //call init
    filter.init(objects);
    
    //create threads
    int nrOfTasks = filter.getNrOfTasks();
    for(int i=0;i<nrOfTasks;i+=step){
      threads.add(new Runner(this, filter, i, i+step<nrOfTasks ? i+step : nrOfTasks));
    }
    
    //start first n of them
    for(int i=0;i<tpSize;i++){
      threads.get(i).start();
    } 
    threadInx = tpSize;
    
    //wait for finalization
    for(Runner runner : threads) {
      try {
        runner.join();
      }
      catch(InterruptedException e) {
       de.lmu.ifi.dbs.elki.logging.LoggingUtil.exception(e);
      }
    }
    
    return filter.cleanUpAndReturn();
  }
  
  private void notifyEndOfExecution() {
    if(threadInx < threads.size()){
      threads.get(threadInx++).start();
    }
  }

  private class Runner extends Thread {

    ParallelFilterRunner parent;
    ParallelFilter task;
    int beginIndex;
    int endIndex;

    public Runner(ParallelFilterRunner parent, ParallelFilter task, int beginIndex, int endIndex) {
      super();
      this.parent = parent;
      this.task = task;
      this.beginIndex = beginIndex;
      this.endIndex = endIndex;
    }

    @Override
    public final void run() {
      for(int i = beginIndex; i < endIndex; i++) {
        task.processTask(i);
      }
      parent.notifyEndOfExecution();
    }
  }

  public static class Parameterizer extends AbstractParameterizer {

    protected static final OptionID THREAD_POOL_S = new OptionID("parallel.threadPoolSize", "Nr of concurrent threads");
    protected int tpSize = 1;
    protected static final OptionID MAX_MEM = new OptionID("parallel.maxMem", "Max. amount of memory to be used [MB], 0==infinite");
    protected long memSize = 0;
    protected static final OptionID FILTER = new OptionID("parallel.filter", "Parallelized filter implementation to be used");
    protected ParallelFilter filter = null;
    protected static final OptionID STEP = new OptionID("parallel.step", "Nr tasks to be executed at once");
    protected int step = 1;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);

      IntParameter tpSizeP = new IntParameter(THREAD_POOL_S, 1);
      tpSizeP.addConstraint(new GreaterEqualConstraint(1));
      if(config.grab(tpSizeP)) {
        tpSize = tpSizeP.getValue();
      }

      LongParameter maxMemP = new LongParameter(MAX_MEM, 0);
      maxMemP.addConstraint(new GreaterEqualConstraint(0));
      if(config.grab(maxMemP)) {
        memSize = maxMemP.getValue();
      }

      final ObjectParameter filterP = new ObjectParameter<>(FILTER, ParallelFilter.class);
      if(config.grab(filterP)) {
        filter = (ParallelFilter) filterP.instantiateClass(config);
      }

      IntParameter stepP = new IntParameter(STEP, 1);
      tpSizeP.addConstraint(new GreaterEqualConstraint(1));
      if(config.grab(stepP)) {
        step = stepP.getValue();
      }
    }

    @Override
    protected ParallelFilterRunner makeInstance() {
      return new ParallelFilterRunner(tpSize, memSize, filter, step);
    }
  }
}
