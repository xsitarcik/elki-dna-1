package de.lmu.ifi.dbs.elki.distance.distancefunction.stuba;
/*
 This file is part of ELKI:
 Environment for Developing KDD-Applications Supported by Index-Structures

 Copyright (C) 2016
 Ludwig-Maximilians-Universität München
 Lehr- und Forschungseinheit für Datenbanksysteme
 ELKI Development Team

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.HashMap;

import de.lmu.ifi.dbs.elki.data.type.SimpleTypeInformation;
import de.lmu.ifi.dbs.elki.distance.distancefunction.AbstractPrimitiveDistanceFunction;
import de.lmu.ifi.dbs.elki.distance.distancefunction.PrimitiveDistanceFunction;
import de.lmu.ifi.dbs.elki.distance.distancefunction.minkowski.EuclideanDistanceFunction;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.ObjectParameter;

/**
 * Distance function that saves already computed results in a hashmap to avoid duplicate computations
 * @author Tomas Farkas
 */
public class CachingDistanceFunction<O> extends AbstractPrimitiveDistanceFunction<O> {

  private PrimitiveDistanceFunction<O> levelDistanceFunction;
  private HashMap<Object, HashMap<Object, Double>> cache = new HashMap<Object, HashMap<Object, Double>>();
  
  @Override
  public SimpleTypeInformation<? super O> getInputTypeRestriction() {
    //same as the nested distance function 
    return levelDistanceFunction.getInputTypeRestriction();
  } 
  
  public CachingDistanceFunction() {}
  
  protected CachingDistanceFunction(PrimitiveDistanceFunction<O> levelDistanceFunction) {
    this.levelDistanceFunction = levelDistanceFunction;
  }  

  @Override
  public double distance(O o1, O o2) { 
    if(o1 instanceof Comparable && o2 instanceof Comparable){
      return distanceComparable(o1, o2);
    }

    HashMap<Object, Double> row;
    //try get by o1
    if((row = cache.get(o1)) != null){
      Double distance;
      if((distance = row.get(o2)) != null){
        return distance;
      }
    }
    //compute
    double distance = levelDistanceFunction.distance(o1, o2);
    //save by o1, o2
    if(row == null){
      row = new HashMap<Object, Double>();
      cache.put(o1, row);
    }
    row.put(o2, distance);
    //save by o2, o1
    if((row = cache.get(o2)) == null){
      row = new HashMap<Object, Double>();
      cache.put(o2, row);
    }
    row.put(o1, distance);
    
    return distance;
  }
  
  @SuppressWarnings({ "unchecked", "rawtypes" })
  private double distanceComparable(O o1, O o2) { 
    Object minID, maxID;
    if(((Comparable)o1).compareTo((Comparable)o2) < 0){
      minID = o1;
      maxID = o2;
    }else{
      minID = o2;
      maxID = o1;
    }
   
    HashMap<Object, Double> row;
    //try get by o1
    if((row = cache.get(minID)) != null){
      Double distance;
      if((distance = row.get(maxID)) != null){
        return distance;
      }
    }
    //compute
    double distance = levelDistanceFunction.distance(o1, o2);
    //save by minID, maxID
    if(row == null){
      row = new HashMap<Object, Double>();
      cache.put(minID, row);
    }
    row.put(maxID, distance);   
    
    return distance;
  }
    
  /**
   * 
   * @author Tomas Farkas
   */
  public static class Parameterizer<O, S extends PrimitiveDistanceFunction<? super O>> extends AbstractParameterizer {
    
    public static final OptionID SIMILARITY_FUNCTION_ID = new OptionID("adapter.distancefunction", "Distance function to derive the distance between database objects from.");
    
    protected S distanceFunction = null;
  
    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);
      final ObjectParameter<S> param = new ObjectParameter<>(SIMILARITY_FUNCTION_ID, PrimitiveDistanceFunction.class, EuclideanDistanceFunction.class);
      if(config.grab(param)) {
        distanceFunction = param.instantiateClass(config);
      }
    }   
    
    @Override
    protected CachingDistanceFunction<?> makeInstance() {
      return new CachingDistanceFunction(distanceFunction);
    }    
   
  }

}

