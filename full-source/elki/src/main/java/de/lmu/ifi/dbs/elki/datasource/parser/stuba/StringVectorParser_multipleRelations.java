package de.lmu.ifi.dbs.elki.datasource.parser.stuba;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.lmu.ifi.dbs.elki.data.type.TypeUtil;
import de.lmu.ifi.dbs.elki.datasource.bundle.MultipleObjectsBundle;
import de.lmu.ifi.dbs.elki.datasource.parser.Parser;
import de.lmu.ifi.dbs.elki.utilities.documentation.Description;
import de.lmu.ifi.dbs.elki.utilities.documentation.Title;
import de.lmu.ifi.dbs.elki.utilities.exceptions.AbortException;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.OptionID;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameterization.Parameterization;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.Flag;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.parameters.PatternParameter;

/**
 * Parser to parse input lines to a separate String fields
 * @author Tomas Farkas
 */
@Title("String Parser")
@Description("Parses new line separated strings")
public class StringVectorParser_multipleRelations implements Parser {
  /**
   * Comment pattern.
   */
  Matcher comment;
  
  /**
   * Col sep pattern
   */
  Pattern colSep;

  /**
   * Flag to trim whitespace.
   */
  boolean trimWhitespace;

  public StringVectorParser_multipleRelations(Pattern comment, Pattern colSep, boolean trimWhitespace) {
    super();
    this.comment = (comment != null) ? comment.matcher("") : null;
    this.colSep = colSep;
    this.trimWhitespace = trimWhitespace;
  }

  @Override
  public MultipleObjectsBundle parse(InputStream in) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
    MultipleObjectsBundle result = null;
    int lineNumber = 0;
    try {
      for(String line; (line = reader.readLine()) != null; lineNumber++) {
        // Skip empty lines and comments
        if(line.length() <= 0 || (comment != null && comment.reset(line).matches())) {
          continue;
        }
        if(lineNumber == 0){//the 1st time
          result = new MultipleObjectsBundle();
          String[] lineAsArray = colSep.split(line);
          for(int i=0;i<lineAsArray.length;i++) { //create empty cols
            result.appendColumn(TypeUtil.STRING, new ArrayList<>());
          }
        }
        String[] lineAsArray = colSep.split(line);
        if(trimWhitespace){
          for(int i=0;i<lineAsArray.length;i++) {
            lineAsArray[i] = lineAsArray[i].trim();
          }
        }
        try{
          result.appendSimple((Object)lineAsArray);
        }catch(AbortException e){
          throw new IllegalArgumentException("Error while parsing line (wrong number of entries in):" + lineNumber + ".");
        }
      }
    }
    catch(IOException e) {
      throw new IllegalArgumentException("Error while parsing line " + lineNumber + ".");
    }     
    
    return result;
  }

  @Override
  public void cleanup() {
    comment.reset("");
  }
  
 
  /**
   * Parameterization class.
   * 
   * @author Felix Stahlberg
   * @author Erich Schubert
   * @author Tomas Farkas
   * 
   * @apiviz.exclude
   */
  public static class Parameterizer extends AbstractParameterizer {
    
   
    public static final String DEFAULT_SEPARATOR = "\\s*[,;\\s]\\s*";
    public static final String DEFAULT_COMMENT = "^\\s*#.*$";

    /**
     * OptionID for the column separator parameter (defaults to whitespace as in
     * {@link #DEFAULT_SEPARATOR}.
     */
    public static final OptionID COLUMN_SEPARATOR_ID = new OptionID("parser.colsep", "Column separator pattern. The default assumes whitespace separated data.");
    
    /**
     * Comment pattern.
     */
    public static final OptionID COMMENT_ID = new OptionID("string.comment", "Ignore lines in the input file that satisfy this pattern.");
    
    /**
     * Flag to trim whitespace.
     */
    public static final OptionID TRIM_ID = new OptionID("string.trim", "Remove leading and trailing whitespace from each line.");

    /**
     * Comment pattern.
     */
    Pattern comment = null;    

    /**
     * Stores the column separator pattern
     */
    protected Pattern colSep = null;

    /**
     * Flag to trim whitespace.
     */
    boolean trimWhitespace = false;

    @Override
    protected void makeOptions(Parameterization config) {
      super.makeOptions(config);
      
      PatternParameter colParam = new PatternParameter(COLUMN_SEPARATOR_ID, DEFAULT_SEPARATOR);
      if(config.grab(colParam)) {
        colSep = colParam.getValue();
      }
      
      PatternParameter commentP = new PatternParameter(COMMENT_ID, DEFAULT_COMMENT);
      if(config.grab(commentP)) {
        comment = commentP.getValue();
      }

      Flag trimP = new Flag(TRIM_ID);
      if(config.grab(trimP)) {
        trimWhitespace = trimP.isTrue();
      }
    }

    @Override
    protected StringVectorParser_multipleRelations makeInstance() {
      return new StringVectorParser_multipleRelations(comment, colSep, trimWhitespace);
    }
  }
}
