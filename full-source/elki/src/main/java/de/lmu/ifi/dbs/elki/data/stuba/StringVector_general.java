package de.lmu.ifi.dbs.elki.data.stuba;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

import de.lmu.ifi.dbs.elki.utilities.datastructures.arraylike.ArrayAdapter;
import de.lmu.ifi.dbs.elki.utilities.io.ByteBufferSerializer;
import de.lmu.ifi.dbs.elki.utilities.optionhandling.AbstractParameterizer;

/** 
 *  StringVector implementation creating a general vector of String values
 * @author Tomas Farkas
 */
public class StringVector_general implements StringVector {

  public static final StringVector_general.Factory STATIC = new StringVector_general.Factory();
  public static final ByteBufferSerializer<StringVector_general> VARIABLE_SERIALIZER = new StringSerializer();

  protected final String[] values;

  
  protected StringVector_general(String[] values, boolean nocopy) {
    if (nocopy) {
      this.values = values;
    } else {
      this.values = new String[values.length];
      System.arraycopy(values, 0, this.values, 0, values.length);
    }
  }
  
  public StringVector_general (String[] values){
    this.values = values.clone();
  }
  
  @Override
  public int getDimensionality() {
    return values.length;
  }
  
  @Override
  public String getValue(int dimension) {
    return values[dimension];
  }

  @Override
  public String[] toArray() {
    return values;
  }


  public String toString(){
    return Arrays.toString(values);
  }
  
  public static class Parameterizer extends AbstractParameterizer {
    @Override
    protected StringVector_general.Factory makeInstance() {
      return STATIC;
    }
  }
   
  /**
   * 
   * @author Tomas Farkas
   *
   */
  public static class Factory implements StringVector.Factory<StringVector_general> {
       
    @Override
    public ByteBufferSerializer<StringVector_general> getDefaultSerializer() {
      return new StringSerializer();//VARIABLE_SERIALIZER;
    }

    @Override
    public Class<? super StringVector_general> getRestrictionClass() {
      return StringVector_general.class;
    }

    @Override
    public <A> StringVector_general newFeatureVector(A array, ArrayAdapter<? extends String, A> adapter) {
      int dim = adapter.size(array);
      String[] values = new String[dim];
      for (int i = 0; i < dim; i++) {
        values[i] = adapter.get(array, i);
      }
      return new StringVector_general(values, true);
    }     

    @Override
    public StringVector_general newStringVector(String[] values) {
      return new StringVector_general(values);       
    }

    @Override
    public StringVector_general newStringVector(List<String> values) {
      String[] array = values.toArray(new String[0]);
      return new StringVector_general(array);
    }

    @Override
    public StringVector_general newStringVector(StringVector values) {
      return new StringVector_general(values.toArray());
    }
  }
  
  /** 
   * @author Tomas Farkas  
   */
  public static class StringSerializer implements ByteBufferSerializer<StringVector_general> {
    
    @Override
    public StringVector_general fromByteBuffer(ByteBuffer buffer) throws IOException {
      String data = buffer.toString();
      String[] values = data.split(">delim<");
      return new StringVector_general(values, true);
    }

    @Override
    public void toByteBuffer(ByteBuffer buffer, StringVector_general vec) throws IOException {
      for(String s :vec.values) {
        buffer.put(s.getBytes());
        buffer.put(">delim<".getBytes());
      }
    }

    @Override
    public int getByteSize(StringVector_general vec) {
      int count = 0;
      for(String s : vec.values){
        count += s.length() + 7;
      }
      return count;
    }
  }
  
}
